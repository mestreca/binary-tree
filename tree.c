#include "tp4_abr.h"

T_Noeud *abr_creer_noeud(int valeur){
    T_Noeud *new = malloc(sizeof(T_Noeud));
    new->val = valeur;
    new->fils_d = NULL;
    new->fils_g = NULL;
    
    return new;
}

void affichage(T_Noeud *node){
    printf("Noeud --> %d\n", node->val);
    printf("\t FG : ");
    if(node->fils_g != NULL)
        printf("%d", node->fils_g->val);
    if(node->fils_g == NULL)
        printf("NULL");
    printf(" -- FD : ");
    if(node->fils_d != NULL)
        printf("%d\n", node->fils_d->val);
    if(node->fils_d == NULL)
        printf("NULL\n");
}

void abr_prefixe(T_Arbre abr){ //On va faire une fonction récursive de parcours
    T_Arbre p_aux = abr;
    
    if(p_aux != NULL){
        affichage(p_aux);
        abr_prefixe(p_aux->fils_g);
        abr_prefixe(p_aux->fils_d);
    }

}

void abr_inserer(int valeur, T_Arbre *abr){ //Passage par adresse puisse modifier l'abre
     T_Arbre p_aux = *abr; //p_aux est un pointeur sur un arbre (donc un pointeur de pointeur) C'est le noeud courant
     T_Arbre p = NULL; // C'est le parent du noeud courant qui à la base est nulle car père de la racine
    
    if((p_aux) == NULL){ //Si l'abre est nul on va le créer
         *abr = abr_creer_noeud(valeur);
    }
    
    else{
        while((p_aux) != NULL){
            
            if( (p_aux)->val == valeur){
                printf("\n## Erreur l'élément existe déjà ! ##\n");
                return;
            }
            
            else if( (p_aux)->val > valeur ){ //On va aller à gauche
                p = p_aux; //On déplace le père au niveau du noeud courant
                (p_aux) = (p_aux)->fils_g;
            }
            
            else{ //On va aller à droite
                p = p_aux; //On déplace le père au niveau du noeud courant
                (p_aux) = (p_aux)->fils_d;
            }
        } //Fin du while
        //Ici on est forcément sur un NULL mais on connait le parent du noeud à insérer
        
        if(valeur > (p)->val){ //On va insérer à droite
            (p)->fils_d = abr_creer_noeud(valeur);
        }
        if(valeur < (p)->val){ //On va insérer à gauche
            (p)->fils_g = abr_creer_noeud(valeur);
        }
        
    }
    
}

void abr_supprimer(int valeur, T_Arbre *abr){
    T_Arbre p_aux = *abr; //p_aux est un pointeur sur un arbre (donc un pointeur de pointeur) C'est le noeud courant
    T_Arbre p = NULL; // C'est le parent du noeud courant qui à la base est nulle car père de la racine
    
    if((p_aux) == NULL){
        printf("### L'arbre est vide ! Rien à supprimer !###\n");
        return;
    }
    
    else{
        while(p_aux != NULL && p_aux->val != valeur){ //On va parcourir notre arbre
            
            if( (p_aux)->val > valeur ){ //On va aller à gauche
                p = p_aux; //On déplace le père au niveau du noeud courant
                (p_aux) = (p_aux)->fils_g;
            }
            
            else { //On va aller à droite
                p = p_aux; //On déplace le père au niveau du noeud courant
                (p_aux) = (p_aux)->fils_d;
            }
        } //Fin du while
    
        if(p_aux == NULL){
            printf("\n### L'élément n'existe pas ! On ne peut pas le supprimer... ###\n\n");
            return;
        }
        
        /*if(p_aux->fils_d == NULL && p_aux->fils_g == NULL && p == NULL){//Pas de fils
            printf("### L'arbre contient une seule valeur... On la supprime.\n");
            abr = NULL;
            return;
        }*/
        
        
        if( (p_aux)->val == valeur){ //p_aux doit être supprimé -> On choisit de faire le raccord avec le plus petit du sous arbre droit (dans cas où il a deux fils)
            
            //Cas si c'est une feuille
            if(p_aux->fils_d == NULL && p_aux->fils_g == NULL){
                if(p == NULL){
                    printf("Je comprend qu'on veut supprimer l'unique noeud de l'arbre");
                    (*abr) = NULL;
                    return;
                }
                
               else if(p_aux->val > p->val){ //p_aux est un fils droit
                   printf("Je comprend qu'on veut supprimer une feuille droite");
                    p->fils_d = NULL;
                   return;
                }
                else if(p_aux->val < p->val) { //C'est un fils gauche
                    printf("Je comprend qu'on veut supprimer une feuille gauche");

                    p->fils_g = NULL;
                    return;
                }
            } //Fin du IF pour le cas de la suppression d'une feuille
            
            //Cas où il possède deux fils => Ici on va faire une fonction recursive -> On doit chercher le plus petit éléments à droite
            else if(p_aux->fils_d != NULL && p_aux->fils_g != NULL){
                T_Arbre n_min = p_aux->fils_d;
                T_Arbre pere_n_min = p_aux; //On va chercher le père de n_min
                
                
                //Probleme à partir de cet endroit ?
                while(n_min->fils_g != NULL){ //On va chercher le noeud le plus petit à droite de p_aux ATTENTION : ce n'est pas forcement une feuille
                    pere_n_min = n_min; //On doit faire avancer le père en même temps que le fils
                    n_min = n_min->fils_g;
                }
                
                if(n_min == p_aux->fils_d){ //Noeud imediatement à droite du noeud à supprimer
                    p_aux->val = n_min->val;
                    p_aux->fils_d = n_min->fils_d;
                }
                    
                else{ //Noeud le plus petit à droite n'est pas son fils droit directement
            
                    //Au cas où n_min ne soit pas une feuille et possède un fils_d
                    p_aux->val = n_min->val;
                    pere_n_min->fils_g = n_min->fils_d;
                }
               
            }
            
            //Cas où il possède un seul fils
            else{
                if(p == NULL){// Le Noeud (p_aux) à supprimer est la racine on doit le traiter comme un cas à part
                    if(p_aux->fils_d != NULL) {(*abr) = p_aux->fils_d;}
                    
                    else{(*abr) = p_aux->fils_g;}
                    
                    return;
                }
                
                else if(p_aux->fils_d != NULL){ //Il possède un unique fils qui est droit
                    if(p_aux->val > p->val){ //p_aux est un fils droit
                        p->fils_d = p_aux->fils_d;
                    }
                    else{ //C'est un fils gauche
                        p->fils_g = p_aux->fils_d;
                    }
                }
                
                else if (p_aux->fils_g != NULL) { //Il possède un unique fils qui est gauche
                    if(p_aux->val > p->val){ //p_aux est un fils droit
                        p->fils_d = p_aux->fils_g;
                    }
                    else{ //C'est un fils gauche
                        p->fils_g = p_aux->fils_g;
                }
            }
        }
            
            
    }//Fin du if pour dire que p_aux pointe sur l'élément à supprimer
        
    
    }//Fin du else arbre non nul
}//Fin de la fonction

    
    
void abr_clone(T_Arbre original, T_Arbre *clone, T_Noeud *parents){
    
    if(original != NULL){ //Condition d'arret de notre fonction recursive
    (*clone) = abr_creer_noeud(original->val); //On creer un nouveau noeud, il est alloué dynamiquement
    T_Arbre p_aux = *clone;
    
    if(parents != NULL){ //Cas different de la racine
        
            if(p_aux->val > parents->val){
                parents->fils_d = (*clone);
            }
            else{
                parents->fils_g = (*clone);
            }
            abr_clone(original->fils_g, &(*clone)->fils_g , *clone); //Recursive sur sous arbre gauche
            abr_clone(original->fils_d, &(*clone)->fils_d, *clone);  //Recursive sur sous arbre droit
       
    }
    
        else{ //C'est le premier noeud (parents == NULL)
            abr_clone(original->fils_g, &(*clone)->fils_g , *clone); //Recursive sur sous arbre gauche
            abr_clone(original->fils_d, &(*clone)->fils_d, *clone);  //Recursive sur sous arbre droit
        }
    }//Fin if
}
    
    
     
