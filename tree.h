#ifndef tp4_abr_h
#define tp4_abr_h

#include <stdio.h>
#include <stdlib.h>

typedef struct Noeud{
    int val;
    struct Noeud *fils_g;
    struct Noeud *fils_d;
}T_Noeud;

typedef T_Noeud *T_Arbre;




T_Noeud *abr_creer_noeud(int valeur);
void affichage(T_Noeud *node);
void abr_prefixe(T_Arbre abr);
void abr_inserer(int valeur, T_Arbre *abr);
void abr_supprimer(int valeur, T_Arbre *abr);
void abr_clone(T_Arbre original, T_Arbre *clone, T_Noeud *parents);



#endif
