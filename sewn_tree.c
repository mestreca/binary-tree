#include "tp4_cousu.h"




T_Noeud_C *cousu_creer_noeud(int valeur){
    
    T_Noeud_C *new=malloc(sizeof(T_Noeud_C));
    new->val=valeur;
    new->fils_g=NULL;
    new->fils_d=NULL;
    new->droit=1;
    new->gauche=1;
    
    return new;
}

void cousu_affichage(T_Noeud_C *node){
    printf("Noeud --> %d\n", node->val);
    
    printf("\t  --> FG(%d) : ",node->gauche);
    if(node->gauche == 0) //Il a un fils
        printf("%d", node->fils_g->val);
    if(node->gauche == 1){
        if(node->fils_g == NULL){ //Cas de l'extremité gauche
            printf("NULL");
        }
        else{
            printf("%d", node->fils_g->val);
        }
    }
    
    printf(" -- FD(%d) : ",node->droit);
    if(node->droit == 0) //Il a un "vrai" fils droit
        printf("%d\n", node->fils_d->val);
    
    if(node->droit == 1){
        if(node->fils_d == NULL)
            printf("NULL\n");
        else{
             printf("%d\n", node->fils_d->val);
        }
    }
    printf("\n");
}


void cousu_prefixe(T_Arbre_C arbre){
    T_Arbre_C p_aux = arbre;
    
    if(p_aux == NULL){
        printf("Veuillez d'abord creer l'arbre !\n");
    }
    
    
    if(p_aux->droit == 1 && p_aux->gauche == 0){ //Cas où il y'a un "vrai" fils gauche mais pas un "vrai" droit
        cousu_affichage(p_aux);
        cousu_prefixe(p_aux->fils_g);
    }
    
    if(p_aux->droit == 0 && p_aux->gauche == 1){ //Cas où il y'a un "vrai" fils droit mais pas un "vrai" gauche
        cousu_affichage(p_aux);
        cousu_prefixe(p_aux->fils_d);
    }
    
    if(p_aux->droit == 0 && p_aux->gauche == 0){ //Cas où il y'a un "vrai" fils gauche et un "vrai" droit
        cousu_affichage(p_aux);
        cousu_prefixe(p_aux->fils_g);
        cousu_prefixe(p_aux->fils_d);
    }
    
    if(p_aux->gauche == 1 && p_aux->droit == 1){
        cousu_affichage(p_aux);
    }
}




void cousu_inserer(int valeur, T_Arbre_C *arbre){
    T_Arbre_C paux = *arbre; //p_aux est un pointeur sur un arbre (donc un pointeur de pointeur) C'est le noeud courant
    T_Arbre_C p = NULL; // C'est le parent du noeud courant qui à la base est nulle car père de la racine
 
    if((paux) == NULL){ //Si l'abre est nul on va le créer
        *arbre = cousu_creer_noeud(valeur);
    }
    
    else{
        while(paux!=NULL){

            if( (paux)->val == valeur){
                printf("Erreur l'element existe deja !\n");
                return;
            }
            
            else if( (paux)->val > valeur ){
                //On va aller à gauche
                if (paux->gauche==1){
                    
                    p=paux;
                    (paux) = (paux) ->fils_g;
                    p->gauche = 0;
                    paux=cousu_creer_noeud(valeur);
                    paux->fils_d=p;
                    paux->fils_g=p->fils_g;
                    p->fils_g=paux;
                    return;
                }//on sort de la boucle
                else{
                    p = paux; //On déplace le père au niveau du noeud courant
                    (paux) = (paux)->fils_g;
                    
                }
            }
            
            else{ //On va aller à droite
                if (paux->droit==1){
                    p=paux;
                    (paux) = (paux) ->fils_d;
                    p->droit = 0;
                    paux=cousu_creer_noeud(valeur);
                    paux->fils_d=p->fils_d; //le fils droit du fils c'est l'ancien fils droit du pere
                    paux->fils_g=p; //le fuls gauche du fils c'est son père
                    p->fils_d=paux; //mnt on met le fils droit du pere égal au fils
                    printf("\n\n");
                    return;
                    
                } //on sort de la boucle
                else{
                    p = paux; //On déplace le père au niveau du noeud courant
                    (paux) = (paux)->fils_d;
                    }
                
            }
            
        }
        
    }

}


void cousu_infixe(T_Arbre_C arbre){ //Affichage en utilisant cette fois ci un parcours infixe de l'arbre
    T_Arbre_C p_aux = arbre; //p_aux est un pointeur sur un arbre (donc un pointeur de pointeur) C'est le noeud courant
   // T_Arbre_C p = NULL; // C'est le parent du noeud courant qui à la base est nulle car père de la racine
    
    while(p_aux->fils_g != NULL){
        p_aux = p_aux->fils_g;
    }//On se situe sur le noeud le plus à gauche de l'arbre
    
    while(p_aux != NULL){
        cousu_affichage(p_aux); //On affiche le noeud courant
        if(p_aux->droit == 1){//Son fils droit à un successeur infixe
            p_aux = p_aux->fils_d;
         }
         else{
              p_aux = p_aux->fils_d;
              while(p_aux->gauche != 1){
                 p_aux = p_aux->fils_g;
             }//On est sur le noeud le plus à gauche du sous arbre
         }
    }
    
}

void abr_to_cousu(T_Arbre abr, T_Arbre_C *clone, T_Noeud_C *parents){
    if(abr != NULL){
        (*clone) = cousu_creer_noeud(abr->val); //On creer un nouveau noeud, il est alloué dynamiquement
    
    T_Arbre_C p_aux = *clone; //On note p_aux afin de le manipuler plus simplement;
    
        if(parents != NULL){ //Cas différent de la racine
            if(p_aux->val < parents->val){ //Cas d'un fils gauche
                parents->gauche = 0; //Il va maintenant posséder un "vrai" noeud gauche
                p_aux->fils_d = parents;
                p_aux->fils_g = parents->fils_g; //Pointe sur anciennement ce que pointait le fils gauche du père
                parents->fils_g = p_aux; //Le père pointe maintenant sur le nouveau noeud
            }
            if(p_aux->val > parents->val){ //Cas d'un fils droit
                parents->droit = 0; //Il va maintenant posséder un "vrai" noeud droit
                p_aux->fils_g = parents;
                p_aux->fils_d = parents->fils_d;
                parents->fils_d = p_aux;
            }
            //On appelle ensuite la fonction sur le sous arbre gauche et droit
            abr_to_cousu(abr->fils_g, &p_aux->fils_g, p_aux);
            abr_to_cousu(abr->fils_d, &p_aux->fils_d, p_aux);
        }
        
            else{ //parents == NULL c'est le premier noeud
                abr_to_cousu(abr->fils_g, &p_aux->fils_g, p_aux);
                abr_to_cousu(abr->fils_d, &p_aux->fils_d, p_aux);
            }
    }//Fin du premier IF
}
