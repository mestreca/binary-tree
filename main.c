#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4_abr.h"
#include "tp4_cousu.h"

int main(int argc, const char * argv[]) {
   
    printf("#################################################################\n# Bienvenue dans l'Application du TP3 - NF16 par TOUZEAU/MESTRE #\n#################################################################\n");

    T_Arbre A = NULL; //On déclare en amont les trois arbres qui vont nous servir
    T_Arbre A_clone = NULL; // Mais on considère qu'un Arbre à valeur "NULL"
    T_Arbre_C A_cousu = NULL; // est un arbre qui n'existe pas encore
    
    
    int choix_menu =0; //entier qui nous sert pour notre menu de selection des actions
    
    while(1){
        printf("\n### Quelle action souhaitez vous réaliser ? ###\n\n");
        printf("1. Creer un ABR\n");
        printf("2. Afficher l'ABR en prefixe\n");
        printf("3. Insérer une valeur dans l'ABR\n");
        printf("4. Supprimer une valeur de l'ABR\n");
        printf("5. Cloner l'ABR\n");
        printf("6. Afficher le clone en prefixe\n");
        printf("7. Creer un arbre binaire cousu à partir d'un ABR\n");
        printf("8. Afficher l'arbre binaire cousu en prefixe\n");
        printf("9. Afficher l'arbre binaire cousu en infixe\n");
        printf("10. Inserer une valeur dans l'abre binaire cousu\n");
        printf("Votre choix :\t");
        scanf("%d",&choix_menu);
    
        switch(choix_menu){
            case 1:
            {//On demande dans un premier temps à l'utilisateur de confirmer son choix
                printf("\n### ATTENTION : La création d'un arbre entraine ici qu'on repart à 0 ###\n");
                int n ;
                do{
                printf("# Veuillez choisir entre 0 (continuer) et 1 (sortir) :\t");
                scanf("%d", &n);
                }while(n!=0 && n!= 1);
                
                if(n == 1){break;} //Si on ne veut pas repartir à zéro alors on break
                
                else{
                //On va créer un arbre
                A = NULL;
                printf("Quel racine voullez vous donner à votre arbre ?\t");
                int racine;
                scanf("%d", &racine);
                abr_inserer(racine, &A);
                break;
                }
            }
                
            case 2:
            {//On affiche l'abre en prefixe
                if(A == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! ###\n");
                    break;
                }
                printf("\n");
                abr_prefixe(A);
                break;
            }
                
            case 3:
            {//On ajoute une valeur à l'abre
                if(A == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! ###\n");
                    break;
                }
                printf("Quel valeur voullez vous ajouter à votre arbre ?\t");
                int val;
                scanf("%d", &val);
                abr_inserer(val, &A);
                break;
            }
                
            case 4:
            {//On va supprimer une valeur de l'abre
                if(A == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! ###\n");
                    break;
                }
                printf("Quelle valeur souhaitez vous supprimer ?"); //Ici affiche un message d'erreur si la valeur n'est pas dans l'arbre
                int val;
                scanf("%d", &val);
                abr_supprimer(val, &A);
                break;
            }
            
            case 5:
            {//On va cloner l'arbre
                if(A == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! ###\n");
                    break;
                }
                printf("\n### Clonage de l'abre en cours... ###\n");
                abr_clone(A, &A_clone, NULL); //Le noeud parent est initialisé à NULL pour le premier appel (fonction récursive)
                break;
            }
                
            case 6:
            {//On va afficher le clone en prefixe
                if(A_clone == NULL){
                    printf("\n### Veuillez d'abord cloner l'arbre ! ###\n");
                    break;
                }
                abr_prefixe(A_clone);
                break;
            }
                
            case 7:
            {//Creer un arbre binaire cousu à partir d'un arbre binaire
                if(A == NULL){
                    printf("\n### L'abre à cloner n'existe pas ! ###\n");
                    break;
                }
                abr_to_cousu(A, &A_cousu, NULL);
                break;
            }
                
            case 8:
            {//On va afficher l'arbre binaire cousu en prefixe
                if(A_cousu == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! Ou le cloner... ###\n");
                    break;
                }
                printf("\n");
                cousu_prefixe(A_cousu);
                break;
            }
                
            case 9:
            {//On va afficher l'arbre binaire cousu en infixe
                if(A_cousu == NULL){
                    printf("\n### Veuillez d'abord creer l'arbre ! Ou le cloner... ###\n");
                    break;
                }
                printf("\n");
                cousu_infixe(A_cousu);
                break;
            }
            
            case 10:
            {//On veut insérer une valeur dans l'abre binaire cousu
                if(A == NULL){ // -> On considère ici cette étape comme la création de notre arbre au cas où il n'existe pas encore
                    printf("\n### Création de l'arbre...\n   Quelle valeur de racine souhaitez vous ?\t");
                    int racine;
                    scanf("%d", &racine);
                    cousu_inserer(racine, &A_cousu);
                    break;
                }
                else{
                    printf("# Quelle valeur souhaitez vous ajouter ? :\t");
                    int val;
                    scanf("%d",&val);
                    cousu_inserer(val, &A_cousu);
                    break;
                }
            }
            
            default :
                return 0;
            break;
                
        }
    
}
    
    return 0;
}
