#ifndef tp4_cousu_h
#define tp4_cousu_h

#include <stdio.h>
#include <stdlib.h>
#include "tp4_abr.h"


struct Noeud_C{
    int val;
    struct Noeud_C *fils_g;
    struct Noeud_C *fils_d;
    int droit;
    int gauche;
};

typedef struct Noeud_C T_Noeud_C;

typedef T_Noeud_C *T_Arbre_C;

T_Noeud_C *cousu_creer_noeud(int valeur);
void cousu_affichage(T_Noeud_C *node);
void cousu_prefixe(T_Arbre_C arbre);
void cousu_inserer(int valeur, T_Arbre_C *arbre);
void cousu_infixe(T_Arbre_C arbre);
void abr_to_cousu(T_Arbre abr, T_Arbre_C *clone, T_Noeud_C *parents);






#endif /* tp4_cousu_h */
